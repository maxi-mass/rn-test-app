import React, { useState } from "react";
import { StyleSheet, View, Alert } from "react-native";
import { AppLoading } from "expo";

import { Navbar } from "./src/components/Navbar";
import { MainScreen } from "./src/screens/MainScreen";
import { TodoScreen } from "./src/screens/TodoScreen";
import * as Font from "expo-font";

async function loadApplication() {
  await Font.loadAsync({
    'roboto-regular': require("./assets/fonts/Roboto-Regular.ttf"),
    "roboto-bold": require("./assets/fonts/Roboto-Bold.ttf")
  });
}

export default function App() {
  const [isReady, setIsReady] = useState(false);
  const [todoId, setTodoId] = useState(null);
  const [todos, setTodos] = useState([
    {
      id: "1",
      title: "Тестовое сообщение для проверки шрифта"
    }
  ]);

  if (!isReady) {
    return (
      <AppLoading
        startAsync={loadApplication}
        onError={err => console.log(err)}
        onFinish={() => setIsReady(true)}
      />
    );
  }

  const addTodo = title => {
    const newTodo = {
      id: Date.now().toString(),
      title: title
    };
    setTodos(prev => [newTodo, ...prev]);
  };

  const deleteTodo = id => {
    let todo = todos.find(t => t.id === id);
    Alert.alert(
      "Удаление элемента",
      `Вы уверены, что хотите удалить элемент "${todo.title}"?`,
      [
        {
          text: "Отмена",
          style: "cancel"
        },
        {
          text: "Да",
          onPress: () => setTodos(prev => prev.filter(item => item.id !== id))
        }
      ],
      { cancelable: false }
    );
  };

  const saveTodo = todo => {
    setTodos(prev =>
      prev.map(prevItem => (prevItem.id === todo.id ? todo : prevItem))
    );
  };

  let content = (
    <MainScreen
      todos={todos}
      addTodo={addTodo}
      deleteTodo={deleteTodo}
      setTodoId={setTodoId}
    />
  );

  if (todoId) {
    const todo = todos.find(item => item.id === todoId);
    content = (
      <TodoScreen
        goBack={() => setTodoId(null)}
        todo={todo}
        onDelete={deleteTodo}
        setTodoId={setTodoId}
        saveTodo={saveTodo}
      />
    );
  }
  return (
    <View>
      <Navbar title="Todo app" />
      <View style={styles.container}>{content}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    paddingVertical: 20
  }
});
