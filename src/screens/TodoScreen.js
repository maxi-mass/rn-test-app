import React, { useState } from "react";
import { View, StyleSheet, Button } from "react-native";
import { THEME } from "../theme";
import { AppCard } from "../components/ui/AppCard";
import { EditModal } from "../components/EditModal";
import { AppTextBold } from './../components/ui/AppTextBold';
import { AppButton } from "../components/ui/AppButton";
import { FontAwesome, AntDesign } from '@expo/vector-icons';

export const TodoScreen = ({ todo, goBack, onDelete, setTodoId, saveTodo }) => {
  const [modal, setModal] = useState(false);

  const onPressHandler = () => {
    onDelete(todo.id);
    setTodoId(null);
  };

  return (
    <View>
      <EditModal
        todo={todo}
        visible={modal}
        onClose={() => setModal(false)}
        onSave={saveTodo}
      />
      <AppCard style={styles.card}>
        <AppTextBold style={styles.title}>{todo.title}</AppTextBold>
        <AppButton onPress={() => setModal(true)}>
          <FontAwesome name='edit' size={20} />
        </AppButton>
      </AppCard>
      <View style={styles.buttons}>
        <View style={styles.button}>
          <AppButton color={THEME.GREY_COLOR} onPress={goBack} >
            <AntDesign name='back' size={20} />
          </AppButton>
        </View>
        <View style={styles.button}>
          <AppButton
            color={THEME.DANGER_COLOR}
            onPress={onPressHandler}
          >
            <FontAwesome name='remove' size={20} />
          </AppButton>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  buttons: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  button: {
    width: "45%"
  },
  title: {
    fontSize: 20
  },
  card: {
    flexDirection: "column",
    marginBottom: 20,
    padding: 15
  }
});
