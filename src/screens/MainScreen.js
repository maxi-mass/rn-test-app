import React from "react";
import { View, StyleSheet, FlatList, Image } from "react-native";
import { AddTodo } from "../components/AddTodo";
import { Todo } from "../components/Todo";

export const MainScreen = ({ addTodo, deleteTodo, todos, setTodoId }) => {
  let content = (
    <FlatList
      data={todos}
      renderItem={({ item }) => (
        <Todo onEdit={setTodoId} onDelete={deleteTodo} todo={item} />
      )}
      keyExtractor={item => item.id}
    />
  );
  if (todos.length === 0) {
    content = (
      <View style={styles.imageWrap}>
        <Image
          style={styles.image}
          source={require("../../assets/no-items.png")}
        />
        {/* <Image
          style={styles.image}
          source={{
            uri: `https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png`
          }}
        /> */}
      </View>
    );
  }
  return (
    <View>
      <View style={styles.addBlock}>
        <AddTodo onAdd={addTodo} />
      </View>
      <View>{content}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  addBlock: {
    marginBottom: 15
  },
  imageWrap: {
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
    height: 300
  },
  image: {
    width: "100%",
    height: "100%",
    resizeMode: "contain"
  }
});
