import React, { useState } from "react";
import {
  Modal,
  View,
  StyleSheet,
  TextInput,
  Alert
} from "react-native";
import { THEME } from "../theme";
import { AppButton } from './ui/AppButton';
import { MaterialCommunityIcons, Ionicons } from '@expo/vector-icons';

export const EditModal = ({ todo, visible, onClose, onSave }) => {
  const [value, setValue] = useState(todo.title);
  const saveHandler = () => {
    if (value.trim().length !== 0) {
      onSave({
        id: todo.id,
        title: value
      });
      onClose();
    } else {
      Alert.alert("Поле не может быть пустым");
    }
  };
  return (
    <Modal animationType="slide" transparent={false} visible={visible}>
      <View style={styles.wrap}>
        <TextInput
          style={styles.input}
          placeholder="Введите название"
          autoCapitalize="none"
          autoCorrect={false}
          maxLength={64}
          value={value}
          onChangeText={setValue}
        />
        <View style={styles.buttons}>
          <AppButton
            onPress={onClose}
            color={THEME.DANGER_COLOR}
          ><MaterialCommunityIcons name='cancel' size={20} /></AppButton>
          <AppButton onPress={saveHandler} >
            <Ionicons name='ios-save' size={20} />
          </AppButton>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  input: {
    padding: 10,
    borderBottomColor: THEME.MAIN_COLOR,
    borderBottomWidth: 2,
    width: "80%"
  },
  buttons: {
    width: "100%",
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-around"
  }
});
