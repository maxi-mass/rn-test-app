import React, { useState } from "react";
import { View, StyleSheet, TextInput, Alert, Keyboard } from "react-native";
import { THEME } from "../theme";
import { MaterialIcons } from '@expo/vector-icons';

export const AddTodo = ({ onAdd }) => {
  const [title, setTitle] = useState("");
  const pressHandler = () => {
    if (title.trim().length !== 0) {
      onAdd(title);
      setTitle("");
      Keyboard.dismiss();
    } else {
      Alert.alert(
        "Ошибка",
        "Значение поля не может быть",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    }
  };

  return (
    <View style={styles.block}>
      <TextInput
        autoCorrect={true}
        autoCapitalize="none"
        style={styles.input}
        value={title}
        onChangeText={setTitle}
        keyboardType="default"
      />
      <MaterialIcons.Button onPress={pressHandler} name='note-add'>
        Добавить
      </MaterialIcons.Button>
    </View>
  );
};

const styles = StyleSheet.create({
  block: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  input: {
    paddingBottom: 3,
    width: "60%",
    borderStyle: "solid",
    borderBottomWidth: 2,
    borderBottomColor: THEME.MAIN_COLOR
  },
  button: {
    borderRadius: 10
  }
});
