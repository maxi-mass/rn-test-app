import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { AppText } from '../components/ui/AppText';

export const Todo = ({ todo, onDelete, onEdit }) => {
  return (
    <TouchableOpacity
      activeOpacity={0.08}
      onPress={() => onEdit(todo.id)}
      onLongPress={onDelete.bind(null, todo.id)}
    >
      <View style={styles.todo}>
        <AppText style={styles.title}>{todo.title}</AppText>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  todo: {
    flexDirection: "row",
    alignItems: "center",
    padding: 15,
    borderWidth: 1,
    borderColor: "grey",
    borderRadius: 10,
    marginBottom: 5
  }
});
